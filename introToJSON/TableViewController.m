//
//  TableViewController.m
//  introToJSON
//
//  Created by Aquiles Alfaro on 12/16/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import "TableViewController.h"



@interface TableViewController ()



@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.i = 0;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:[NSURL URLWithString:@"http://www.toyocat.net/ricardo.json"]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data == nil){
            NSLog(@"No Data Received ");
        } else {
            NSLog(@"Received data need to parse JSON");
            NSLog(@"JSON Data:%@", data);
            
            NSError *error = nil;
            id object = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            if (error){
                NSLog(@"Error no data!");
                return;
            }
            
            if ([object isKindOfClass:[NSArray class]] ){
                 self.acct = object;
                
                
                NSLog(@"Accounts: %@", self.acct);
                
            }
            
            
            
            
        }
    }];
    
    [task resume];
    
    // refresh the table view
    [self.tableView reloadData];
    
    NSLog(@"New Data: %@", self.acct);
    
    //NSLog(@"TempHold: %@", );
   
}


    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"Accounts: %@", self.acct);
    NSLog(@"Number: %lu", [self.acct count]);
    return [self.acct count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell.textLabel setText:[NSString stringWithFormat:@"Acct#:%@", [self.acct[self.i] valueForKey:@"accountNumber"]]];
    
   
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"Bal:$%@", [self.acct[self.i] valueForKey:@"accountBalance"]]];
    
    if(self.i < [self.acct count]-1){

        self.i = self.i + 1;
    }else{
        if(self.i == [self.acct count]-1){
            self.i = 0;
        }
    }
   return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)loadData:(UIBarButtonItem *)sender {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:[NSURL URLWithString:@"http://www.toyocat.net/ricardo.json"]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data == nil){
            NSLog(@"No Data Received ");
        } else {
            NSLog(@"Received data need to parse JSON");
            NSLog(@"JSON Data:%@", data);
            
            NSError *error = nil;
            id object = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            if (error){
                NSLog(@"Error no data!");
                return;
            }
            
            if ([object isKindOfClass:[NSArray class]] ){
                TableViewController *myAccount = [[TableViewController alloc]init];
                myAccount.acct = object;
                
                
                //NSLog(@"Accounts: %@", myAccount.acct);
                
            }
            
            
            
            
        }
    }];
    
    [task resume];
    
    // refresh the table view
    [self.tableView reloadData];
    
}
    


@end
